#Serializer-code-py

The objective of this project is save pieces of python codes (python scripts ?) for later use.

We will use Django with sqlite3. In the folder **project** is the Django project.

##Libraries founded: 

###Pickle
The [pickle module](https://docs.python.org/3/library/pickle.html) implements binary protocols for serializing and 
de-serializing a Python object structure.

Founded django-picklefield:<br/>
"[django-picklefield](https://pypi.org/project/django-picklefield/) provides an implementation of a pickled object field.
Such fields can contain any picklable objects".

###dill
The [dill package](https://pypi.org/project/dill/) extends python’s pickle module for serializing and de-serializing 
python objects to the majority of the built-in python types. 


###Joblib
The [Joblib package](https://joblib.readthedocs.io/en/latest/index.html) main purpose is pipelining python jobs, but it 
also provide saving/loading python objects methods


##Studies

Did use cases with **django-picklefield** and **dill** package. Implement the use cases through the Django test.

In the file **test.py** there are a usage tests. In the folder project/picklefield_app/ for django-picklefield and
in folder project/dill_app/ for dill.

Initially they were tested to save and load ints, strings; both implementations passed successfully, the same with  lists, dicts.
After tried with simple objects, and no problem was noticed.

###Limitations
The limitations appeared when they were tested with big objects (> 1 GB). For these cases used large numpy zeros. <br/>
Results:
- **dill**: Failed to store a 1.2 GB object, with the follow error:
```
django.db.utils.InterfaceError: Error binding parameter 0 - probably unsupported type.
```
Before this error: some 70Mb, 100Mb, 150Mb, 200, 300Mb, 430Mb, 600Mb, 880Mb objects was saved.
So, the problem would be the size of the 1.2 GB object

- **django-picklefield**: failed to store 880Mb object with the same error.<br/>
But save object the size of  70Mb, 100Mb, 150Mb, 200, 300Mb, 430Mb, 600Mb.

###Solutions
Two possible solutions to the problem were studied, one for each library

- **dill**: after dumps a big object, divide the serialized object in a little chunks. The solution for this idea is in 
the folder project/multi_dill_app/ <br/>
This solution works well with 900Mb, 1.1Gb, 1.3Gb, 1.6Gb, 1.9Gb objects. With objects bigger than 2Gb pycharm sends 
sigkill.


- **django-picklefield**: this package has the option for compress the serialized objects, this option will tested.
This solution works well with 760Mb, 900Mb, 1.1Gb, 1.3Gb, 1.6Gb, 2.3Gb objects. With objects bigger than 2.5Gb pycharm sends 
sigkill.

So, both solutions work well.

---

##ZeroMQ
Sends the serialized objects through zeromq sockets. In the file **test.py** there are a usage tests. 
In the folder project/zeromq_app/ 

Works well sending basic elements, arrays, dicts and objects.