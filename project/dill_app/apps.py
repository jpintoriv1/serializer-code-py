from django.apps import AppConfig


class DillAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dill_app'
