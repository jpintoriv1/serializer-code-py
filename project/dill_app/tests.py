from django.test import TestCase
from .models import ScriptModel, SerializedModel
import dill as pickle
import random
import numpy as np
import sys
import traceback


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def double_age(self):
        self.age = self.age * 2


class TestObject:
    def __init__(self, size):
        self.size = size
        self.array_ = [0] * size

    def randomize_array(self):
        for i in range(0, self.size):
            self.array_[i] = random.randint(1, self.size)

    def get_array(self):
        return self.array_


class BasicVariables(TestCase):

    def setUp(self):
        script = ScriptModel(name="script1.py")
        script.save()

        source = "Hello world"
        serialized = pickle.dumps(source)
        serialized_model = SerializedModel(serialized=serialized,
                                           script=script)
        serialized_model.save()

        source = 3 + 5
        serialized = pickle.dumps(source)
        serialized_model = SerializedModel(serialized=serialized,
                                           script=script)
        serialized_model.save()

    def test1(self):
        script = ScriptModel.objects.get(name="script1.py")
        self.assertEqual(script.name, "script1.py")

        serialized_set = SerializedModel.objects.filter(script=script)

        loaded = pickle.loads(serialized_set[0].serialized)
        self.assertEqual(loaded, "Hello world")

        loaded = pickle.loads(serialized_set[1].serialized)
        self.assertEqual(loaded, 8)


class ListsDicts(TestCase):

    def setUp(self):
        script = ScriptModel(name="script2.py")
        script.save()

        source = [1, 2, "hello", "4", True]
        serialized = pickle.dumps(source)
        serialized_model = SerializedModel(serialized=serialized,
                                           script=script)
        serialized_model.save()

        source = {
            "Topic": "serialized",
            "Year": 2021,
            "list": [1, 2, 3, 5, 8],
            "assert": False
        }
        serialized = pickle.dumps(source)
        serialized_model = SerializedModel(serialized=serialized,
                                           script=script)
        serialized_model.save()

    def test_list(self):
        script = ScriptModel.objects.get(name="script2.py")

        serialized_set = SerializedModel.objects.filter(script=script)
        list_ = pickle.loads(serialized_set[0].serialized)
        self.assertEqual(list_[0], 1)
        self.assertEqual(list_[1], 2)
        self.assertEqual(list_[2], "hello")
        self.assertEqual(list_[3], "4")
        self.assertTrue(list_[4])

    def test_dict(self):
        script = ScriptModel.objects.get(name="script2.py")

        serialized_set = SerializedModel.objects.filter(script=script)
        dict_ = pickle.loads(serialized_set[1].serialized)
        self.assertEqual(dict_["Topic"], "serialized")
        self.assertEqual(dict_["Year"], 2021)
        self.assertEqual(dict_["list"], [1, 2, 3, 5, 8])
        self.assertFalse(dict_["assert"])


class ClassPerson(TestCase):

    def setUp(self):
        script = ScriptModel(name="script3.py")
        script.save()

        source = Person("Jorge", 26)
        serialized = pickle.dumps(source)
        serialized_model = SerializedModel(serialized=serialized,
                                           script=script)
        serialized_model.save()

        source = Person("John", 32)
        source.double_age()
        serialized = pickle.dumps(source)
        serialized_model = SerializedModel(serialized=serialized,
                                           script=script)
        serialized_model.save()

    def test_person(self):
        script = ScriptModel.objects.get(name="script3.py")

        serialized_set = SerializedModel.objects.filter(script=script)

        person = pickle.loads(serialized_set[0].serialized)
        self.assertEqual(person.get_name(), "Jorge")
        self.assertEqual(person.get_age(), 26)

        person = pickle.loads(serialized_set[1].serialized)
        self.assertEqual(person.get_age(), 64)
        self.assertEqual(person.get_name(), "John")
        person.double_age()
        self.assertEqual(person.get_age(), 128)


class ClassTestObject(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script4.py")
        self.script.save()

        instance_ = TestObject(5000)
        self.serialized = pickle.dumps(instance_)
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

        instance_ = TestObject(1000)
        instance_.randomize_array()
        self.serialized = pickle.dumps(instance_)
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

    def test_arrays(self):
        script = ScriptModel.objects.get(name="script4.py")

        serialized_set = SerializedModel.objects.filter(script=script)

        test_object = pickle.loads(serialized_set[0].serialized)
        for value in test_object.get_array():
            self.assertEqual(value,0)
        test_object.randomize_array()
        for value in test_object.get_array():
            self.assertGreater(value, 0)

        test_object = pickle.loads(serialized_set[1].serialized)
        for value in test_object.get_array():
            self.assertGreater(value, 0)


class NumpyExamples(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script_numpy.py")
        self.script.save()

    def test_zeros(self):

        factor = 3000
        while True:
            print("Size zeros : " + str(factor) + " x " + str(factor))

            source = np.zeros((factor, factor))
            size = sys.getsizeof(source) / (1024 ** 2)
            print("Size array: " + str(size) + " mb")

            serialized = pickle.dumps(source)
            size = sys.getsizeof(serialized) / (1024 ** 2)
            print("Size serialized: " + str(size) + " mb")

            serialized_model = SerializedModel(serialized=serialized,
                                               script=self.script)

            try:
                # serialized_model.clean_fields()
                serialized_model.save()
                print("Ok Serialized")
            except Exception as e:
                print("Fail serialized :")
                print(e)
                traceback.print_exc()
                break

            del source
            del serialized
            del serialized_model
            factor = int(factor * 1.2)
    '''        
    def test_zeros(self):

        factor = 3000
        while True:
            print("Size zeros : " + str(factor) + " x " + str(factor))

            source = np.zeros((factor, factor))
            size = sys.getsizeof(source) / (1024 ** 2)
            print("Size array: " + str(size) + " mb")

            serialized = pickle.dumps(source)
            size = sys.getsizeof(serialized) / (1024 ** 2)
            print("Size serialized: " + str(size) + " mb")

            serialized_model = SerializedModel(serialized=serialized,
                                               script=self.script)

            try:
                serialized_model.save()
                print("Ok Serialized")
            except Exception as e:
                print("Fail serialized :")
                print(e)
                traceback.print_exc()
                break

            del source
            factor = int(factor * 1.1)
    '''
