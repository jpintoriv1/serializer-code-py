from django.db import models
import sys
import funcy


# Create your models here.
class ScriptModel(models.Model):
    name = models.CharField(max_length=50)


class SerializedListModel(models.Model):
    serialized = models.BinaryField(max_length=16777215)


# trying to LIMITS the maximum size of serialized
# https://dev.mysql.com/doc/refman/8.0/en/column-count-limit.html
# https://django-mysql.readthedocs.io/en/latest/model_fields/resizable_text_binary_fields.html#
# we want to work with 16 mb - MEDIUMBLOB in django-mysql.
# https://wiki.postgresql.org/wiki/BinaryFilesInDB postgresql maximum 1GB of binary
class SerializedModel(models.Model):
    script = models.ForeignKey(ScriptModel, on_delete=models.CASCADE)
    serialized_list = models.ManyToManyField(SerializedListModel)




