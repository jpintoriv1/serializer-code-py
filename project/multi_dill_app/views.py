#from django.shortcuts import render
from .models import SerializedModel, SerializedListModel
import funcy


def save_serialized(bytes_, script):
    serialized_model = SerializedModel(script=script)
    serialized_model.save()
    serialized_list = list(funcy.chunks(16777215, bytes_))
    for value in serialized_list:
        serialized_list_value = SerializedListModel(serialized=value)
        serialized_list_value.save()
        serialized_model.serialized_list.add(serialized_list_value)
    serialized_model.save()

    return serialized_model


def get_serialized(serialized_model):
    ans_array = b''
    for value in serialized_model.serialized_list.all():
        ans_array += value.serialized
    return bytes(ans_array)
