from django.apps import AppConfig


class MultiDillAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'multi_dill_app'
