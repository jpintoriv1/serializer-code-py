from django.test import TestCase
import numpy as np
import dill as pickle
from .views import save_serialized, get_serialized
import sys
from .models import ScriptModel, SerializedModel


# Create your tests here.
class InitialTest(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script_numpy.py")
        self.script.save()
    '''
    # test serialize objects smaller than 16Mb
    def test_save_serialized_1(self):

        size_array = 512

        while True:
            size_array = int(size_array * 1.1)
            source = np.zeros((size_array, size_array))
            size = sys.getsizeof(source) / (1024 ** 2)
            if size > 15:
                del source
                break

            print("Size array: " + str(size_array) + " x " + str(size_array))
            print("Size array: " + str(size) + " Mb")

            serialized = pickle.dumps(source)
            size = sys.getsizeof(serialized) / (1024 ** 2)
            print("Size serialized array: " + str(size) + " Mb")

            saved = True
            try:
                save_serialized(serialized, self.script)
            except Exception as e:
                saved = False
                print("Fail serialized :")
                print(e)

            self.assertTrue(saved, "Cannot save the serialized object")

            del source
    
    # test serialize objects bigger than
    def test_save_serialized_2(self):

        size_array = 10000

        while True:
            size_array = int(size_array * 1.1)
            source = np.zeros((size_array, size_array))
            size = sys.getsizeof(source) / (1024 ** 2)
            if size > 2500:
                del source
                break

            print("Size array: " + str(size_array) + " x " + str(size_array))
            print("Size array: " + str(size) + " Mb")

            serialized = pickle.dumps(source)
            size = sys.getsizeof(serialized) / (1024 ** 2)
            print("Size serialized array: " + str(size) + " Mb")

            del source
            saved = True
            try:
                save_serialized(serialized, self.script)
                print("serialized !")
            except Exception as e:
                saved = False
                print("Fail serialized :")
                print(e)

            self.assertTrue(saved, "Cannot save the serialized object")

            del serialized
        '''
    def test_save_and_load(self):
        size_array = 10000
        source = np.zeros((size_array, size_array))
        serialized = pickle.dumps(source)
        size = sys.getsizeof(serialized) / (1024 ** 2)
        print("Size array: " + str(size) + " Mb")
        save_serialized(serialized, self.script)

        serialized_ = SerializedModel.objects.filter(script=self.script)[0]
        serialized_ = get_serialized(serialized_)

        object_ = pickle.loads(serialized_)

        self.assertTrue(isinstance(object_, np.ndarray))
        self.assertTrue(np.all(object_ == 0))
        self.assertEquals(object_.size, size_array*size_array)