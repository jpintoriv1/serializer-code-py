from django.db import models
from picklefield.fields import PickledObjectField


class ScriptModel(models.Model):
    name = models.CharField(max_length=50)


class SerializedModel(models.Model):
    serialized = PickledObjectField(compress=True)
    script = models.ForeignKey(ScriptModel, on_delete=models.CASCADE)