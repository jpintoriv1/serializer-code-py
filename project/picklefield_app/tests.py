from django.test import TestCase
from .models import ScriptModel, SerializedModel
import numpy as np
import traceback
import sys
import random


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def double_age(self):
        self.age = self.age*2


class TestObject:
    def __init__(self, size):
        self.size = size
        self.array_ = [0] * size

    def randomize_array(self):
        for i in range(0, self.size):
            self.array_[i] = random.randint(1,self.size)

    def get_array(self):
        return self.array_


class BasicVariables(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script1.py")
        self.script.save()
        self.serialized1 = "Hello world"
        self.serialized1_model = SerializedModel(serialized=self.serialized1,
                                                 script=self.script)
        self.serialized1_model.save()
        self.serialized2 = 3 + 5
        self.serialized2_model = SerializedModel(serialized=self.serialized2,
                                                 script=self.script)
        self.serialized2_model.save()

    def test1(self):
        script = ScriptModel.objects.get(name="script1.py")
        self.assertEqual(script.name, "script1.py")

        serialized_set = SerializedModel.objects.filter(script=script)
        self.assertEqual(serialized_set.count(), 2)
        self.assertEqual(serialized_set[0].serialized, "Hello world")
        self.assertEqual(serialized_set[1].serialized, 8)


class ListsDicts(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script2.py")
        self.script.save()

        self.serialized = [1, 2, "hello", "4", True]
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

        self.serialized = {
            "Topic": "serialized",
            "Year": 2021,
            "list": [1, 2, 3, 5, 8],
            "assert": False
        }
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

    def test_list(self):
        script = ScriptModel.objects.get(name="script2.py")

        serialized_set = SerializedModel.objects.filter(script=script)
        list_ = serialized_set[0].serialized
        self.assertEqual(list_[0], 1)
        self.assertEqual(list_[1], 2)
        self.assertEqual(list_[2], "hello")
        self.assertEqual(list_[3], "4")
        self.assertTrue(list_[4])

    def test_dict(self):
        script = ScriptModel.objects.get(name="script2.py")

        serialized_set = SerializedModel.objects.filter(script=script)
        dict_ = serialized_set[1].serialized
        self.assertEqual(dict_["Topic"], "serialized")
        self.assertEqual(dict_["Year"], 2021)
        self.assertEqual(dict_["list"], [1, 2, 3, 5, 8])
        self.assertFalse(dict_["assert"])


class ClassPerson(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script3.py")
        self.script.save()

        instance_ = Person("Jorge", 26)
        self.serialized = instance_
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

        instance_ = Person("John", 32)
        instance_.double_age()
        self.serialized = instance_
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

    def test_person(self):
        script = ScriptModel.objects.get(name="script3.py")

        serialized_set = SerializedModel.objects.filter(script=script)

        person = serialized_set[0].serialized
        self.assertEqual(person.get_name(), "Jorge")
        self.assertEqual(person.get_age(), 26)

        person = serialized_set[1].serialized
        self.assertEqual(person.get_age(), 64)
        self.assertEqual(person.get_name(), "John")
        person.double_age()
        self.assertEqual(person.get_age(),128)


class ClassTestObject(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script4.py")
        self.script.save()

        instance_ = TestObject(5000)
        self.serialized = instance_
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

        instance_ = TestObject(1000)
        instance_.randomize_array()
        self.serialized = instance_
        self.serialized_model = SerializedModel(serialized=self.serialized,
                                                script=self.script)
        self.serialized_model.save()

    def test_arrays(self):
        script = ScriptModel.objects.get(name="script4.py")

        serialized_set = SerializedModel.objects.filter(script=script)

        test_object = serialized_set[0].serialized
        for value in test_object.get_array():
            self.assertEqual(value,0)
        test_object.randomize_array()
        for value in test_object.get_array():
            self.assertGreater(value, 0)

        test_object = serialized_set[1].serialized
        for value in test_object.get_array():
            self.assertGreater(value, 0)


class NumpyExamples(TestCase):

    def setUp(self):
        self.script = ScriptModel(name="script_numpy.py")
        self.script.save()

    def test_zeros(self):

        factor = 10000
        while True:
            print("Size zeros: " + str(factor) + " x " + str(factor))

            source = np.zeros((factor, factor))
            size = sys.getsizeof(source) / (1024 ** 2)
            print("Size array: " + str(size) + " mb")

            serialized_model = SerializedModel(serialized=source,
                                               script=self.script)
            del source
            try:
                serialized_model.save()
                print("Ok Serialized")
            except Exception as e:
                print("Fail serialized :")
                print(e)
                traceback.print_exc()
                break

            del serialized_model

            factor = int(factor * 1.1)
