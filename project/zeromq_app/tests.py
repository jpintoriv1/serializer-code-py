from django.test import TestCase
import zmq
from multiprocessing import Process
import dill as pickle
import numpy as np
import base64


def send_messages_json(messages):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5560")

    for value in messages:
        socket.recv()
        socket.send_json(value)

    socket.close()


class JsonExample(TestCase):

    def setUp(self):
        self.objects = list()

        self.objects.append("Hello world")
        self.objects.append(350)
        self.objects.append(np.zeros((1000, 1000)))
        self.objects.append(False)
        messages = [None] * len(self.objects)
        for idx, value in enumerate(self.objects):
            serialized_ = pickle.dumps(value)
            serialized_ = base64.b64encode(serialized_)

            json_ = {"channel": "SERIALISED OBJECT",
                     "payload":
                         {"obj": serialized_.decode()}
                     }
            messages[idx] = json_

        p = Process(target=send_messages_json, args=(messages,))
        p.start()

    def test_1(self):
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect("tcp://localhost:5560")

        for value in self.objects:
            socket.send(b'START')

            message = socket.recv_json()
            obj_ = message["payload"]["obj"].encode()
            obj_ = base64.b64decode(obj_)
            thing = pickle.loads(obj_)

            self.assertEqual(type(thing), type(value))

        socket.close()
