from multiprocessing import Process
import dill as pickle
import numpy as np
import base64
import zmq


def send_messages():
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5560")

    objects_ = list()
    objects_.append("Hello world")
    objects_.append(350)
    objects_.append(np.zeros((500, 500)))
    objects_.append([1, 2, 3, 4, 5])
    objects_.append(False)

    socket.recv()
    print("Start sending")
    for value in objects_:
        serialized_ = pickle.dumps(value)
        serialized_ = base64.b64encode(serialized_)
        serialized_ = serialized_.decode()

        json_ = {"channel": "SERIALISED OBJECT",
                 "payload":
                     {"obj": serialized_}
                 }

        socket.send_json(json_)
        socket.recv()

    socket.close()


def received_messages():
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5560")

    socket.send(b'START')
    while True:
        message = socket.recv_json()
        obj_ = message["payload"]["obj"].encode()
        obj_ = base64.b64decode(obj_)
        thing = pickle.loads(obj_)

        socket.send(b'Received')

        print("received object type:" + str(type(thing)))

        if type(thing) == bool and not thing:
            break

    socket.close()


p = Process(target=send_messages)
p.start()

p = Process(target=received_messages)
p.start()

