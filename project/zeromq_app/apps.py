from django.apps import AppConfig


class ZeromqAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zeromq_app'
